#!/bin/bash -x

APP_NAME="missile_run"
BUILD_DIR="./build"


APP_FONTS_SOURCE="./fonts/LiberationMono-Regular.ttf"
APP_SOUND_SOURCE_DIR="./sounds"
APP_SOUND_BACKGROUND_SOURCE="$APP_SOUND_SOURCE_DIR/skyfire.ogg "
APP_SOUND_FIRE_SOURCE="$APP_SOUND_SOURCE_DIR/laser7.wav"

# copy SFML dylibs  to Frameworks
SFML_SOURCE_DIR=/Users/frank/opt/SFML-2.5.1/lib
SFML_DEST_DIR="$BUILD_DIR/$APP_NAME.app/Contents/Frameworks"

if [ ! -d "$SFML_DEST_DIR" ]; then
    mkdir -p $SFML_DEST_DIR
fi


cp -f $SFML_SOURCE_DIR/libsfml-audio.2.5.1.dylib "$BUILD_DIR/$APP_NAME.app/Contents/Frameworks/"
cp -f $SFML_SOURCE_DIR/libsfml-graphics.2.5.1.dylib "$BUILD_DIR/$APP_NAME.app/Contents/Frameworks/"
cp -f $SFML_SOURCE_DIR/libsfml-network.2.5.1.dylib "$BUILD_DIR/$APP_NAME.app/Contents/Frameworks/"
cp -f $SFML_SOURCE_DIR/libsfml-system.2.5.1.dylib "$BUILD_DIR/$APP_NAME.app/Contents/Frameworks/"
cp -f $SFML_SOURCE_DIR/libsfml-window.2.5.1.dylib "$BUILD_DIR/$APP_NAME.app/Contents/Frameworks/"


# fix dependency of the main app if needed
install_name_tool -change "@rpath/libsfml-audio.2.5.dylib" "@executable_path/../Frameworks/libsfml-audio.2.5.1.dylib" "$BUILD_DIR/$APP_NAME.app/Contents/MacOS/$APP_NAME"
install_name_tool -change "@rpath/libsfml-graphics.2.5.dylib" "@executable_path/../Frameworks/libsfml-graphics.2.5.1.dylib" "$BUILD_DIR/$APP_NAME.app/Contents/MacOS/$APP_NAME"
install_name_tool -change "@rpath/libsfml-network.2.5.dylib" "@executable_path/../Frameworks/libsfml-network.2.5.1.dylib" "$BUILD_DIR/$APP_NAME.app/Contents/MacOS/$APP_NAME"
install_name_tool -change "@rpath/libsfml-system.2.5.dylib" "@executable_path/../Frameworks/libsfml-system.2.5.1.dylib" "$BUILD_DIR/$APP_NAME.app/Contents/MacOS/$APP_NAME"
install_name_tool -change "@rpath/libsfml-window.2.5.dylib" "@executable_path/../Frameworks/libsfml-window.2.5.1.dylib" "$BUILD_DIR/$APP_NAME.app/Contents/MacOS/$APP_NAME"

# fonts

APP_FONT_DEST_DIR="$BUILD_DIR/$APP_NAME.app/Contents/Resources/fonts"

if [ ! -d "$APP_FONT_DEST_DIR" ]; then
    mkdir -p $APP_FONT_DEST_DIR
fi

cp -f $APP_FONTS_SOURCE $APP_FONT_DEST_DIR

# sounds

APP_SOUND_DEST_DIR="$BUILD_DIR/$APP_NAME.app/Contents/Resources/sounds"

if [ ! -d "$APP_SOUND_DEST_DIR" ]; then
    mkdir -p $APP_SOUND_DEST_DIR
fi

cp -f $APP_SOUND_BACKGROUND_SOURCE $APP_SOUND_DEST_DIR
cp -f $APP_SOUND_FIRE_SOURCE $APP_SOUND_DEST_DIR
