missile_run
===========

Classic arcade game , players shooting enemy ships (aliens).

My intro to C++11 game programming using SFML library.

Demonstrates methods for building on and for multiple platforms.

It compiles and runs on

- Mac OSX
- Linux (Fedora 27/28)
- Linux (Cross compile on Fedora/Mingw64, runs on Windows)
- Windows 10 (NMake Makefiles)
- Centos 7 (now using devtools 8 with gcc-8 and c++17 support)
- Windows 10 (MSYS2/Mingw64)
- Arch Linux (5.5.11 kernel)

Commands:

    - Left Arrow (move left)
    - Right Arrow (Move Right)
    - Space Bar (Fire)
    - P (Pause)
    - R (Resume)
    - Q (Quit)


Mac and Linux Instructions
--------------------------

Here is how to build for Linux and Mac. 

cmake
-----

``` text
✔ ~/repos/missile_run/build [master|✚ 1]
18:52 $ cmake ../
-- The C compiler identification is AppleClang 9.0.0.9000039
-- The CXX compiler identification is AppleClang 9.0.0.9000039
-- Check for working C compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc
-- Check for working C compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++
-- Check for working CXX compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- APPLE -----
-- Found SFML .. in /usr/local/include
-- Configuring done
-- Generating done
-- Build files have been written to: /Users/frank/repos/missile_run/build
✔ ~/repos/missile_run/build [master|✚ 1]
```


make
----

``` text
18:53 $ make
Scanning dependencies of target main
[ 10%] Building CXX object CMakeFiles/main.dir/src/main.cpp.o
[ 20%] Building CXX object CMakeFiles/main.dir/src/splash.cpp.o
[ 30%] Building CXX object CMakeFiles/main.dir/src/game.cpp.o
[ 40%] Building CXX object CMakeFiles/main.dir/src/missile.cpp.o
[ 50%] Building CXX object CMakeFiles/main.dir/src/random_generator.cpp.o
[ 60%] Building CXX object CMakeFiles/main.dir/src/player.cpp.o
[ 70%] Building CXX object CMakeFiles/main.dir/src/missiles.cpp.o
[ 80%] Building CXX object CMakeFiles/main.dir/src/bullet.cpp.o
[ 90%] Building CXX object CMakeFiles/main.dir/src/bullets.cpp.o
[100%] Linking CXX executable main
[100%] Built target main
✔ ~/repos/missile_run/build [master|✚ 1]
```



linkage
-------

``` text
21:22 $ otool -L ./missile_run
./missile_run:
/usr/local/opt/sfml/lib/libsfml-graphics.2.4.dylib (compatibility version 2.4.0, current version 2.4.2)
/usr/local/opt/sfml/lib/libsfml-window.2.4.dylib (compatibility version 2.4.0, current version 2.4.2)
/usr/local/opt/sfml/lib/libsfml-system.2.4.dylib (compatibility version 2.4.0, current version 2.4.2)
/usr/local/opt/sfml/lib/libsfml-audio.2.4.dylib (compatibility version 2.4.0, current version 2.4.2)
/usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 400.9.0)
/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1252.0.0)
```


execute
-------

Lets shoot some alien ships !!

``` text
./missile_run
```


Awesome !!


![Screenshot](docs/display1.png)


Notes for Mac build
-------------------

Normally you can run the executable on Mac as built above. However I have taken the
liberty to allow for bundling and packaging in prep for Mac deployment. This means
dylibs and resources must be placed in correct locations within the bundle to meet
Apple's security requirements for resource loading.

So, you can do the following step after building on Mac.

``` text
cd ..
./post_build.sh

```

Then you can run and get sounds also.

``` text
open ./build/missile_run.app/
```


Notes for Windows 10 build
--------------------------

SFML is compiled/installed in local directory.

``` text
C:\Users\Frank\opt\SFML251>tree /A
Folder PATH listing
Volume serial number is XXXX-XXXX
C:.
+---bin
+---include
|   \---SFML
|       +---Audio
|       +---Graphics
|       +---Network
|       +---System
|       \---Window
\---lib
    \---cmake
        \---SFML
```


To build the same game on Windows 10, do the following.

1 Open VS2019 x64 Command Line Tool

2 Change directory to missile_run and create a build directory.

```
cd c:\Users\frank\repos\missile_run

mkdir build

cd build

c:\Users\Frank\repos\missile_run\build>
```

3 Execute cmake.
 Make sure you specify **Release**, to match the release SFML libs, otherwise fonts may not load etc...


``` text
c:\Users\Frank\repos\missile_run\build>cmake -DCMAKE_BUILD_TYPE=Release -G "NMake Makefiles" ../
-- The C compiler identification is MSVC 19.23.28105.4
-- The CXX compiler identification is MSVC 19.23.28105.4
-- Check for working C compiler: C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.23.28105/bin/Hostx64/x64/cl.exe
-- Check for working C compiler: C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.23.28105/bin/Hostx64/x64/cl.exe -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.23.28105/bin/Hostx64/x64/cl.exe
-- Check for working CXX compiler: C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.23.28105/bin/Hostx64/x64/cl.exe -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- WIN32 -----
-- Found SFML 2.5.1 in C:/Users/Frank/opt/SFML251/lib/cmake/SFML
-- Configuring done
-- Generating done
-- Build files have been written to: C:/Users/Frank/repos/missile_run/build

```

4 Run nmake to build

``` text
c:\Users\Frank\repos\missile_run\build>nmake

Microsoft (R) Program Maintenance Utility Version 14.23.28105.4
Copyright (C) Microsoft Corporation.  All rights reserved.

Scanning dependencies of target missile_run
[ 10%] Building CXX object CMakeFiles/missile_run.dir/src/main.cpp.obj
main.cpp
[ 20%] Building CXX object CMakeFiles/missile_run.dir/src/splash.cpp.obj
splash.cpp
[ 30%] Building CXX object CMakeFiles/missile_run.dir/src/game.cpp.obj
game.cpp
[ 40%] Building CXX object CMakeFiles/missile_run.dir/src/missile.cpp.obj
missile.cpp
[ 50%] Building CXX object CMakeFiles/missile_run.dir/src/random_generator.cpp.obj
random_generator.cpp
[ 60%] Building CXX object CMakeFiles/missile_run.dir/src/player.cpp.obj
player.cpp
[ 70%] Building CXX object CMakeFiles/missile_run.dir/src/missiles.cpp.obj
missiles.cpp
[ 80%] Building CXX object CMakeFiles/missile_run.dir/src/bullet.cpp.obj
bullet.cpp
[ 90%] Building CXX object CMakeFiles/missile_run.dir/src/bullets.cpp.obj
bullets.cpp
[100%] Linking CXX executable missile_run.exe
[100%] Built target missile_run
```



5 Check dependencies on shared SFML libs

``` text
c:\Users\Frank\repos\missile_run\build>dumpbin /dependents missile_run.exe

Microsoft (R) COFF/PE Dumper Version 14.23.28105.4
Copyright (C) Microsoft Corporation.  All rights reserved.


Dump of file missile_run.exe

File Type: EXECUTABLE IMAGE

  Image has the following dependencies:

    sfml-graphics-2.dll
    sfml-window-2.dll
    sfml-audio-2.dll
    sfml-system-2.dll
    MSVCP140.dll
    VCRUNTIME140.dll
    VCRUNTIME140_1.dll
    api-ms-win-crt-runtime-l1-1-0.dll
    api-ms-win-crt-heap-l1-1-0.dll
    api-ms-win-crt-math-l1-1-0.dll
    api-ms-win-crt-stdio-l1-1-0.dll
    api-ms-win-crt-locale-l1-1-0.dll
    KERNEL32.dll

  Summary

        1000 .data
        1000 .pdata
       12000 .rdata
        1000 .reloc
        1000 .rsrc
        6000 .text
```



6 ONLY IF MISSING,Copy the the following dll's from SFML install dir
  Cmake should copy them automatically anyway.

```
sfml-graphics-2.dll
sfml-window-2.dll
sfml-system-2.dll
sfml-audio-2.dll
openal32.dll
```

7 Start the executable from the build dir.

```
missile_run.exe
```

Game is now running on Windows 10.

Awesome !!


Centos 7 + Fedora 30
--------------------

The game will also compile and run on Centos 7. Just make sure you install
all the dependencies first. SFML was also build from source, using cmake.

As we are starting to require C++17 as a minimum (and why shouldn't we ?), we need
to enable devtools 8 to grab a later gcc chain (for Centos 7 only, fedora 30 is good to go already).

Enable GCC-8

``` text
15:19 $ rm -rf build/ && mkdir build && cd build/
15:19 $ scl enable devtoolset-8 bash
15:19 $ which gcc
/opt/rh/devtoolset-8/root/usr/bin/gcc

15:20 $ gcc --version
gcc (GCC) 8.3.1 20190311 (Red Hat 8.3.1-3)
Copyright (C) 2018 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

```

Here is Centos 7 example, Fedora 30 is similar.
```

-- The C compiler identification is GNU 8.3.1
-- The CXX compiler identification is GNU 8.3.1
-- Check for working C compiler: /opt/rh/devtoolset-8/root/usr/bin/cc
-- Check for working C compiler: /opt/rh/devtoolset-8/root/usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /opt/rh/devtoolset-8/root/usr/bin/c++
-- Check for working CXX compiler: /opt/rh/devtoolset-8/root/usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- UNIX -----
-- Found SFML 2.5.1 in /home/frank/opt/sfml251/lib/cmake/SFML
-- Configuring done
-- Generating done
-- Build files have been written to: /home/frank/repos/missile_run/build

```



```
23:07 $ make
Scanning dependencies of target missile_run
[ 10%] Building CXX object CMakeFiles/missile_run.dir/src/main.cpp.o
[ 20%] Building CXX object CMakeFiles/missile_run.dir/src/splash.cpp.o
[ 30%] Building CXX object CMakeFiles/missile_run.dir/src/game.cpp.o
[ 40%] Building CXX object CMakeFiles/missile_run.dir/src/missile.cpp.o
[ 50%] Building CXX object CMakeFiles/missile_run.dir/src/random_generator.cpp.o
[ 60%] Building CXX object CMakeFiles/missile_run.dir/src/player.cpp.o
[ 70%] Building CXX object CMakeFiles/missile_run.dir/src/missiles.cpp.o
[ 80%] Building CXX object CMakeFiles/missile_run.dir/src/bullet.cpp.o
[ 90%] Building CXX object CMakeFiles/missile_run.dir/src/bullets.cpp.o
[100%] Linking CXX executable missile_run
[100%] Built target missile_run


```


*ldd* output after building on Centos 7

```
✔ ~/repos/missile_run/build [master|✚ 1⚑ 3] 
15:26 $ ldd ./missile_run 
	linux-vdso.so.1 =>  (0x00007ffdd6bc0000)
	libsfml-graphics.so.2.5 => /home/frank/opt/sfml251/lib/libsfml-graphics.so.2.5 (0x00007f45593d4000)
	libsfml-window.so.2.5 => /home/frank/opt/sfml251/lib/libsfml-window.so.2.5 (0x00007f45591a8000)
	libsfml-audio.so.2.5 => /home/frank/opt/sfml251/lib/libsfml-audio.so.2.5 (0x00007f4558f8d000)
	libsfml-system.so.2.5 => /home/frank/opt/sfml251/lib/libsfml-system.so.2.5 (0x00007f4558d80000)
	libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f4558a79000)
	libm.so.6 => /lib64/libm.so.6 (0x00007f4558777000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f4558561000)
	libc.so.6 => /lib64/libc.so.6 (0x00007f4558193000)
	libGL.so.1 => /lib64/libGL.so.1 (0x00007f4557f07000)
	libGLU.so.1 => /lib64/libGLU.so.1 (0x00007f4557c87000)
	libX11.so.6 => /lib64/libX11.so.6 (0x00007f4557949000)
	libXrandr.so.2 => /lib64/libXrandr.so.2 (0x00007f455773e000)
	libfreetype.so.6 => /lib64/libfreetype.so.6 (0x00007f455747f000)
	libudev.so.1 => /lib64/libudev.so.1 (0x00007f4557269000)
	libopenal.so.1 => /lib64/libopenal.so.1 (0x00007f4556ffa000)
	libvorbisenc.so.2 => /lib64/libvorbisenc.so.2 (0x00007f4556b2b000)
	libvorbisfile.so.3 => /lib64/libvorbisfile.so.3 (0x00007f4556922000)
	libvorbis.so.0 => /lib64/libvorbis.so.0 (0x00007f45566f5000)
	libogg.so.0 => /lib64/libogg.so.0 (0x00007f45564ee000)
	libFLAC.so.8 => /lib64/libFLAC.so.8 (0x00007f45562a9000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f455608d000)
	librt.so.1 => /lib64/librt.so.1 (0x00007f4555e85000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f4559633000)
	libGLX.so.0 => /lib64/libGLX.so.0 (0x00007f4555c53000)
	libXext.so.6 => /lib64/libXext.so.6 (0x00007f4555a41000)
	libGLdispatch.so.0 => /lib64/libGLdispatch.so.0 (0x00007f455578b000)
	libdl.so.2 => /lib64/libdl.so.2 (0x00007f4555587000)
	libxcb.so.1 => /lib64/libxcb.so.1 (0x00007f455535f000)
	libXrender.so.1 => /lib64/libXrender.so.1 (0x00007f4555154000)
	libz.so.1 => /lib64/libz.so.1 (0x00007f4554f3e000)
	libbz2.so.1 => /lib64/libbz2.so.1 (0x00007f4554d2e000)
	libpng15.so.15 => /lib64/libpng15.so.15 (0x00007f4554b03000)
	libcap.so.2 => /lib64/libcap.so.2 (0x00007f45548fe000)
	libdw.so.1 => /lib64/libdw.so.1 (0x00007f45546ad000)
	libXau.so.6 => /lib64/libXau.so.6 (0x00007f45544a9000)
	libattr.so.1 => /lib64/libattr.so.1 (0x00007f45542a4000)
	libelf.so.1 => /lib64/libelf.so.1 (0x00007f455408c000)
	liblzma.so.5 => /lib64/liblzma.so.5 (0x00007f4553e66000)

```


Windows 10 MSYS2/MinGW64
------------------------

Yes you can even build on WIndows using MSYS2/MINGW64 as follows.
Open a MINGW64 Terminal on Windows and execute the following.

Cmake

```
$ cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CC_COMPILER=gcc -DCMAKE_MAKE_PROGRAM=mingw32-make -G"MinGW Makefiles" ../
-- The C compiler identification is GNU 7.3.0
-- The CXX compiler identification is GNU 7.3.0
-- Check for working C compiler: C:/msys64/mingw64/bin/gcc.exe
-- Check for working C compiler: C:/msys64/mingw64/bin/gcc.exe -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: C:/msys64/mingw64/bin/g++.exe
-- Check for working CXX compiler: C:/msys64/mingw64/bin/g++.exe -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- MINGW -----
-- Found SFML 2.4.2 in C:/msys64/mingw64/include
-- Configuring done
-- Generating done
CMake Warning:
  Manually-specified variables were not used by the project:

    CMAKE_CC_COMPILER


-- Build files have been written to: C:/Users/Frank/repos/missile_run/build

```

Compile

```
$ mingw32-make.exe
Scanning dependencies of target missile_run
[ 10%] Building CXX object CMakeFiles/missile_run.dir/src/main.cpp.obj
[ 20%] Building CXX object CMakeFiles/missile_run.dir/src/splash.cpp.obj
[ 30%] Building CXX object CMakeFiles/missile_run.dir/src/game.cpp.obj
[ 40%] Building CXX object CMakeFiles/missile_run.dir/src/missile.cpp.obj
[ 50%] Building CXX object CMakeFiles/missile_run.dir/src/random_generator.cpp.obj
[ 60%] Building CXX object CMakeFiles/missile_run.dir/src/player.cpp.obj
[ 70%] Building CXX object CMakeFiles/missile_run.dir/src/missiles.cpp.obj
[ 80%] Building CXX object CMakeFiles/missile_run.dir/src/bullet.cpp.obj
[ 90%] Building CXX object CMakeFiles/missile_run.dir/src/bullets.cpp.obj
[100%] Linking CXX executable missile_run.exe
[100%] Built target missile_run

```

Linking

```
$ ldd ./missile_run.exe
        ntdll.dll => /c/WINDOWS/SYSTEM32/ntdll.dll (0x7ff88ff30000)
        KERNEL32.DLL => /c/WINDOWS/System32/KERNEL32.DLL (0x7ff88d400000)
        KERNELBASE.dll => /c/WINDOWS/System32/KERNELBASE.dll (0x7ff88cd60000)
        msvcrt.dll => /c/WINDOWS/System32/msvcrt.dll (0x7ff88ded0000)
        libgcc_s_seh-1.dll => /mingw64/bin/libgcc_s_seh-1.dll (0x61440000)
        sfml-audio-2.dll => /mingw64/bin/sfml-audio-2.dll (0x6e700000)
        sfml-system-2.dll => /mingw64/bin/sfml-system-2.dll (0x63c00000)
        sfml-graphics-2.dll => /mingw64/bin/sfml-graphics-2.dll (0x70dc0000)
        sfml-window-2.dll => /mingw64/bin/sfml-window-2.dll (0x6bc00000)
        libwinpthread-1.dll => /mingw64/bin/libwinpthread-1.dll (0x64940000)
        ADVAPI32.dll => /c/WINDOWS/System32/ADVAPI32.dll (0x7ff88dcb0000)
        sechost.dll => /c/WINDOWS/System32/sechost.dll (0x7ff88e900000)
        RPCRT4.dll => /c/WINDOWS/System32/RPCRT4.dll (0x7ff88df70000)
        GDI32.dll => /c/WINDOWS/System32/GDI32.dll (0x7ff88d4b0000)
        libstdc++-6.dll => /mingw64/bin/libstdc++-6.dll (0x6fc40000)
        gdi32full.dll => /c/WINDOWS/System32/gdi32full.dll (0x7ff88d260000)
        USER32.dll => /c/WINDOWS/System32/USER32.dll (0x7ff88e770000)
        msvcp_win.dll => /c/WINDOWS/System32/msvcp_win.dll (0x7ff88cff0000)
        win32u.dll => /c/WINDOWS/System32/win32u.dll (0x7ff88cfd0000)
        ucrtbase.dll => /c/WINDOWS/System32/ucrtbase.dll (0x7ff88c490000)
        libFLAC-8.dll => /mingw64/bin/libFLAC-8.dll (0x64080000)
        libogg-0.dll => /mingw64/bin/libogg-0.dll (0x70680000)
        libopenal-1.dll => /mingw64/bin/libopenal-1.dll (0x66040000)
        ole32.dll => /c/WINDOWS/System32/ole32.dll (0x7ff88fda0000)
        libvorbis-0.dll => /mingw64/bin/libvorbis-0.dll (0x6d540000)
        combase.dll => /c/WINDOWS/System32/combase.dll (0x7ff88d990000)
        bcryptPrimitives.dll => /c/WINDOWS/System32/bcryptPrimitives.dll (0x7ff88cce0000)
        libvorbisenc-2.dll => /mingw64/bin/libvorbisenc-2.dll (0x6b680000)
        SHELL32.dll => /c/WINDOWS/System32/SHELL32.dll (0x7ff88e960000)
        cfgmgr32.dll => /c/WINDOWS/System32/cfgmgr32.dll (0x7ff88c440000)
        libvorbisfile-3.dll => /mingw64/bin/libvorbisfile-3.dll (0x6b3c0000)
        shcore.dll => /c/WINDOWS/System32/shcore.dll (0x7ff88e4f0000)
        WINMM.dll => /c/WINDOWS/SYSTEM32/WINMM.dll (0x7ff88a510000)
        windows.storage.dll => /c/WINDOWS/System32/windows.storage.dll (0x7ff88c590000)
        shlwapi.dll => /c/WINDOWS/System32/shlwapi.dll (0x7ff88e380000)
        kernel.appcore.dll => /c/WINDOWS/System32/kernel.appcore.dll (0x7ff88c2f0000)
        OPENGL32.dll => /c/WINDOWS/SYSTEM32/OPENGL32.dll (0x7ff8868f0000)
        powrprof.dll => /c/WINDOWS/System32/powrprof.dll (0x7ff88c2a0000)
        profapi.dll => /c/WINDOWS/System32/profapi.dll (0x7ff88c280000)
        libfreetype-6.dll => /mingw64/bin/libfreetype-6.dll (0x693c0000)
        libjpeg-8.dll => /mingw64/bin/libjpeg-8.dll (0x6b800000)
        WINMMBASE.dll => /c/WINDOWS/SYSTEM32/WINMMBASE.dll (0x7ff88a4e0000)
        ??? => ??? (0x190000)
        GLU32.dll => /c/WINDOWS/SYSTEM32/GLU32.dll (0x7ff8868c0000)
        libbz2-1.dll => /mingw64/bin/libbz2-1.dll (0x626c0000)
        libharfbuzz-0.dll => /mingw64/bin/libharfbuzz-0.dll (0x61600000)
        libpng16-16.dll => /mingw64/bin/libpng16-16.dll (0x68b40000)
        zlib1.dll => /mingw64/bin/zlib1.dll (0x62e80000)
        libglib-2.0-0.dll => /mingw64/bin/libglib-2.0-0.dll (0x685c0000)
        libgraphite2.dll => /mingw64/bin/libgraphite2.dll (0x70540000)
        WS2_32.dll => /c/WINDOWS/System32/WS2_32.dll (0x7ff88e270000)
        libpcre-1.dll => /mingw64/bin/libpcre-1.dll (0x69140000)
        libintl-8.dll => /mingw64/bin/libintl-8.dll (0x61cc0000)
        libiconv-2.dll => /mingw64/bin/libiconv-2.dll (0x2690000)
        libiconv-2.dll => /mingw64/bin/libiconv-2.dll (0x2690000)

```

Execute

``` text
./missile_run
```

MINGW64 and Clang
-----------------

You can compile also using **clang** under MSYS2/MINGW64 by invoking **cmake**
as follows.

``` text
Frank@win10vm MINGW64 ~/repos/missile_run/build
$ cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CC_COMPILER=clang -DCMAKE_MAKE_PROGRAM=mingw32-make -G"MinGW Makefiles" ../
-- The C compiler identification is GNU 7.3.0
-- The CXX compiler identification is Clang 5.0.1
-- Check for working C compiler: C:/msys64/mingw64/bin/gcc.exe
-- Check for working C compiler: C:/msys64/mingw64/bin/gcc.exe -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: C:/msys64/mingw64/bin/clang++.exe
-- Check for working CXX compiler: C:/msys64/mingw64/bin/clang++.exe -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- MINGW -----
-- Found SFML 2.4.2 in C:/msys64/mingw64/include
-- Configuring done
-- Generating done
CMake Warning:
  Manually-specified variables were not used by the project:

    CMAKE_CC_COMPILER


-- Build files have been written to: C:/Users/Frank/repos/missile_run/build

```


Fedora 27 MinGW64 Cross compile to Windows
------------------------------------------

Use mingw64 on Fedora 27 to cross compile to Windows x86_64 executable.
This uses **mingw64-cmake** and **mingw64-make** .



```
22:43 $ mingw64-cmake -DCMAKE_BUILD_TYPE=Release ../
-- The C compiler identification is GNU 7.2.0
-- The CXX compiler identification is GNU 7.2.0
-- Check for working C compiler: /usr/bin/x86_64-w64-mingw32-gcc
-- Check for working C compiler: /usr/bin/x86_64-w64-mingw32-gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/x86_64-w64-mingw32-g++
-- Check for working CXX compiler: /usr/bin/x86_64-w64-mingw32-g++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- ----- MINGW ON Linux -----
-- CMAKE_HOST_SYSTEM_NAME: Linux
-- Configuring done
-- Generating done
CMake Warning:
  Manually-specified variables were not used by the project:

    BUILD_SHARED_LIBS
    CMAKE_INSTALL_LIBDIR
    INCLUDE_INSTALL_DIR
    SHARE_INSTALL_PREFIX
    SYSCONF_INSTALL_DIR


-- Build files have been written to: /home/frank/repos/missile_run/build

```


Now compile

```
19:34 $ mingw64-make
Scanning dependencies of target missile_run
[ 10%] Building CXX object CMakeFiles/missile_run.dir/src/main.cpp.obj
[ 20%] Building CXX object CMakeFiles/missile_run.dir/src/splash.cpp.obj
[ 30%] Building CXX object CMakeFiles/missile_run.dir/src/game.cpp.obj
[ 40%] Building CXX object CMakeFiles/missile_run.dir/src/missile.cpp.obj
[ 50%] Building CXX object CMakeFiles/missile_run.dir/src/random_generator.cpp.obj
[ 60%] Building CXX object CMakeFiles/missile_run.dir/src/player.cpp.obj
[ 70%] Building CXX object CMakeFiles/missile_run.dir/src/missiles.cpp.obj
[ 80%] Building CXX object CMakeFiles/missile_run.dir/src/bullet.cpp.obj
[ 90%] Building CXX object CMakeFiles/missile_run.dir/src/bullets.cpp.obj
[100%] Linking CXX executable missile_run.exe
[100%] Built target missile_run
✔ ~/repos/missile_run/build [master|✚ 1]
19:34 $
✔ ~/repos/missile_run/build [master|✚ 1]
19:34 $ file ./missile_run.exe
./missile_run.exe: PE32+ executable (console) x86-64, for MS Windows

```

Prepare to run the cross compiled code on Windows 10

Now copy all the required mingw64 libs from Linux to Windows, along with
missile_run.exe, fonts dir and sounds dir. A test directory on Windows 10
containing all the resources would look like this.

``` text

C:\Users\Frank\test>dir
 Volume in drive C has no label.
 Volume Serial Number is CAF1-2A9E

 Directory of C:\Users\Frank\test

27/03/2018  10:05 PM    <DIR>          .
27/03/2018  10:05 PM    <DIR>          ..
27/03/2018  09:00 PM    <DIR>          fonts
27/03/2018  09:44 PM            76,354 libbz2-1.dll
27/03/2018  09:40 PM           287,116 libFLAC-8.dll
27/03/2018  09:36 PM           646,986 libfreetype-6.dll
27/03/2018  09:12 PM           964,014 libgcc_s_seh-1.dll
27/03/2018  09:38 PM           412,134 libjpeg-62.dll
27/03/2018  09:39 PM            31,535 libogg-0.dll
27/03/2018  09:44 PM           222,325 libpng16-16.dll
27/03/2018  09:14 PM        15,574,084 libstdc++-6.dll
27/03/2018  09:43 PM           189,001 libvorbis-0.dll
27/03/2018  09:42 PM           583,467 libvorbisenc-2.dll
27/03/2018  09:43 PM            42,298 libvorbisfile-3.dll
27/03/2018  09:15 PM            58,418 libwinpthread-1.dll
27/03/2018  09:24 PM           208,412 missile_run.exe
27/03/2018  10:03 PM           603,104 OpenAL32.dll
27/03/2018  09:29 PM           221,846 sfml-audio-2.dll
27/03/2018  09:29 PM           468,365 sfml-graphics-2.dll
27/03/2018  09:29 PM           211,402 sfml-network-2.dll
27/03/2018  09:29 PM           141,802 sfml-system-2.dll
27/03/2018  09:29 PM           231,737 sfml-window-2.dll
27/03/2018  09:00 PM    <DIR>          sounds
27/03/2018  09:45 PM            90,697 zlib1.dll

```

You can now launch **missile_run.exe** on WIndows and play the game.

Awesome !!

NOTES:
------

To create GTAGS/GRTAGS/GPATH issue **gtags -v** in project root.

Mac


``` text
rm G*
mkdir gtags_external
cd gtags_external
ln -s ~/opt/SFML-2.5.1/include/ 
cd ..
gtags -v

```

Linux

``` text
rm G*
mkdir gtags_external
cd gtags_external
ln -s /usr/include/SFML . 
cd ..
gtags -v

```


Media Attributions
------------------

Of course every game needs some sound and image resources. Thanks to these
folks for their efforts.

SkyFire background music

* https://opengameart.org/content/space-shooter-music
  
* https://opengameart.org/sites/default/files/SkyFire%20%28Title%20Screen%29.ogg

  Copyright/Attribution Notice: 
  MUSIC BY OBLIDIVM http://oblidivmmusic.blogspot.com.es/ 


Space Shooter Art.

*  https://opengameart.org/content/space-shooter-art
  
*  https://opengameart.org/sites/default/files/spaceArt.zip

  Attribution Instructions: 
  Credit "Kenney.nl" or "www.kenney.nl", this is not mandatory.

