// -*-c++-*-

#ifndef __ALIENS_H
#define __ALIENS_H

#include <cstdint>
#include <vector>
#include <memory>
#include "SFML/Graphics.hpp"
#include "alien.h"

const uint8_t MAX_ALIEN_NUM = 5;


class Aliens {

public:
    void input(void);
    void update(void);
    void render(sf::RenderWindow& window_);

    Aliens();

    std::vector<Alien> vec_aliens_;

private:

};

#endif /* __ALIENS_H */
