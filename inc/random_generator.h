#ifndef __RANDOM_GENERATOR_H
#define __RANDOM_GENERATOR_H

#include "SFML/Graphics.hpp"
#include <random>

class RandomGenerator {
public:
    //static int get_random(const int & min, const int & max);
    static float get_random(const float min, const float max);    
};


#endif /* __RANDOM_GENERATOR_H */
