// -*-c++-*-
#ifndef __PLAYER_CPP_H
#define __PLAYER_CPP_H

// standard
#include <memory>

// 3rd party
#include "SFML/Graphics.hpp"

// local
#include "entity.h"

namespace PlayerContext {
    enum class Command {LEFT, RIGHT, FIRE};
    enum class State {NORMAL, SHIELD, COLLISION, DEAD};
}

class Player : public Entity {

public:

    Player();

    virtual void input(void);
    virtual void update(void);
    virtual void render(sf::RenderWindow& window_);

    void update(PlayerContext::Command cmd);

    uint32_t destroyed_enemy_count_ {0};

private:

};

#endif /* __PLAYER_CPP_H */
