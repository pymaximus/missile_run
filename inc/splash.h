#ifndef __SPLASH_H
#define __SPLASH_H

#include "SFML/Graphics.hpp"

// font relative path
const std::string fontPath {"fonts/LiberationMono-Regular.ttf"};

class Splash
{
public:
    Splash();
    void draw(sf::RenderWindow& window, const std::string& text, float x, float y);

private:
    sf::Font font_;
    sf::Text text_;
};


#endif /* __SPLASH_H */
