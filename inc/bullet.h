// -*-c++-*-

#ifndef __BULLET_H
#define __BULLET_H

// standard
#include <cstdint>
#include <cstddef>
#include <vector>

// local
#include "common.h"
#include "entity.h"


namespace BulletContext {
    enum class State {NORMAL, SHIELD, COLLISION, DEAD};
    enum class RenderMode {BR_YELLOW, BR_RECTANGLE};
}


// bullet data
class Bullet : public Entity {

public:

    BulletContext::RenderMode  render_mode_;

    explicit Bullet(uint16_t id);

    virtual void input(void);
    virtual void update(void);
    virtual void render(sf::RenderWindow& window_);

private:


};

#endif /* __BULLET_H */
