// -*-c++-*-

#ifndef __BULLETS_H
#define __BULLETS_H

#include "player.h"
#include "bullet.h"
#include "stdint.h"
#include "stdbool.h"
#include <vector>

#include "SFML/System.hpp"


class Player;                   // forward declare
class Bullet;

class Bullets {

public:
    void input(void);
    void update(void);
    void render(sf::RenderWindow& window);

    bool respawn(const Player& player);

    Bullets();

    std::vector<Bullet> vec_bullets_;

private:
    sf::Clock clock_;
};

#endif /* __BULLETS_H */
