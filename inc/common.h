// -*-c++-*-

#ifndef __COMMON_H
#define __COMMON_H

namespace Common {
    constexpr float W_WIDTH = 1000;
    constexpr float W_HEIGHT = 1000;
    constexpr float FRAME_INTERVAL = 1;
}; // namespace Common

#endif
