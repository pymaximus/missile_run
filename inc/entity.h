// -*-c++-*-

#ifndef __ENTITY_H
#define __ENTITY_H

// standard
#include <cstdint>
#include <cstddef>
#include <vector>
#include <memory>

// 3rd party
#include "SFML/Graphics.hpp"

// entity data
class Entity {

public:
    uint16_t id_;
    float x_;
    float y_;
    float vx_;
    float vy_;
    bool is_alive_;
    bool has_collided_;
    int16_t health_;

    virtual void input(void)=0;
    virtual void update(void)=0;
    virtual void render(sf::RenderWindow& window_)=0;

    virtual ~Entity() {}

    sf::Sprite sprite_;

protected:

    std::shared_ptr<sf::Texture> texture_;
};

#endif /* __ENTITY_H */
