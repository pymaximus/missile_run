// -*-c++-*-

#ifndef __ALIEN_H
#define __ALIEN_H

// standard
#include <cstdint>
#include <vector>
#include <memory>

// 3rd party
#include "SFML/Graphics.hpp"

// local
#include "entity.h"

namespace AlienContext {
    enum class Command {LEFT, RIGHT, FIRE};
    enum class State {NORMAL, SHIELD, COLLISION, DEAD};
};

// alien data
class Alien : public Entity {
public:

    Alien(uint16_t id);

    virtual void render(sf::RenderWindow& window_);
    virtual void input(void);
    virtual void update(void);


private:

    // supports resetting dead alien to be re-used from pool.
    void reset(void);

};

#endif /* __ALIEN_H */
