// -*-c++-*-

#ifndef __GAME_H
#define __GAME_H

// standard
#include <string>
#include <iostream>
#include <sstream>

// 3rd party
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"

// local
#include "common.h"
#include "player.h"
#include "aliens.h"
#include "bullets.h"
#include "splash.h"
#include "random_generator.h"


namespace GameContext {
    enum class Command {PAUSE, RESUME};
    enum class State {RUNNING, PAUSED, FINISHED};
}


class Game
{
public:
    Game();
    void run();

private:
    void processEvents();
    void update();
    void render();

    sf::RenderWindow window_;

    Player player_;
    Splash splash_;
    Aliens aliens_;
    Bullets bullets_;

    sf::Music music_;
    sf::SoundBuffer fire_sound_buff_;
    sf::Sound fire_sound_;
    sf::Text statusText_;

    GameContext::State state_ {GameContext::State::RUNNING};

    // for displaying game stats
    uint32_t frameCount_ = 0;
    std::ostringstream frameCountStr_;

    // collision detection between entities
    void collision_update_aliens_player();
    void collision_update_aliens_bullets();

    void display_game_stats();
};

#endif
