#include "Game.h"

#ifdef __APPLE__
#include "ResourcePath.hpp"
#endif

enum class GameStates
{
    STATE_START = 1,
    STATE_MENU, // 2
    STATE_OPTIONS, // n + 1
    STATE_SETTINGS,
    STATE_LEVEL,
    STATE_GAME_OVER
    //.. etc
};

Game::Game() : window_(sf::VideoMode(static_cast<int>(Common::W_WIDTH),
                                     static_cast<int>(Common::W_HEIGHT)),
                       "Missile Run - (C) Frank Singleton 2018")
{
    window_.setFramerateLimit(80);
    //window_.setVerticalSyncEnabled(true);


#ifdef __APPLE__
    const auto resource_path = resourcePath();
    std::cout << resource_path << std::endl;

    music_.openFromFile(resource_path + "sounds/skyfire.ogg");
    // load and set fire sound effect
    fire_sound_buff_.loadFromFile(resource_path + "sounds/laser7.wav");
    fire_sound_.setBuffer(fire_sound_buff_);
#else
    music_.openFromFile("sounds/skyfire.ogg");
   // load and set fire sound effect
    fire_sound_buff_.loadFromFile("sounds/laser7.wav");
    fire_sound_.setBuffer(fire_sound_buff_);
#endif

    music_.setVolume(50);
    music_.setLoop(true);
    music_.play();

}

// input/update/render game loop
void Game::run()
{

    // enter game loop
    while (window_.isOpen())
    {
        processEvents();
        if (state_ == GameContext::State::RUNNING) {
            update();
            render();
        }
    }
}

void Game::processEvents()
{
    sf::Event event;
    while (window_.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            window_.close();
            break;
        default:
            // FIXME: handle other events if required
            break;
        }

    }

    // Game State Handling
    switch(state_) {
    case GameContext::State::RUNNING:
        // FIXME: switch on state instead of if statements.

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            player_.update(PlayerContext::Command::FIRE);
            // yuck, but for testing
            if (bullets_.respawn(player_)) {
                // play fire sound if a bullet was respawned
                fire_sound_.setVolume(50);
                fire_sound_.play();
            }
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
            //std::cout << "Closing" << std::endl;
            window_.close();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            //std::cout << "Left" << std::endl;
            player_.update(PlayerContext::Command::LEFT);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            player_.update(PlayerContext::Command::RIGHT);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
        {
            state_ = GameContext::State::PAUSED;
        }
        break;

    case GameContext::State::PAUSED:
        // if not running, can only scan for resume command.
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
        {
            //std::cout << "RESUME" << std::endl;
            state_ = GameContext::State::RUNNING;
        }
        // so we don't peg cpu waiting for Resume
        sf::sleep(sf::milliseconds(200));
        break;

    case GameContext::State::FINISHED:
        // if game finished, can only scan for Quit command.
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        {
            window_.close();
        }
        // so we don't peg cpu waiting for Quit
        sf::sleep(sf::milliseconds(200));
        break;
    default:
        std::cout << "Unhandled Game State" << std::endl;
        break;
    }
}

void Game::update()
{

    aliens_.update();
    bullets_.update();
    collision_update_aliens_bullets();
    collision_update_aliens_player();
}

void Game::render()
{
    window_.clear();

    frameCount_++;
    display_game_stats();

    // entity render
    player_.render(window_);
    aliens_.render(window_);
    bullets_.render(window_);
    window_.display();
}

void Game::display_game_stats() {
    frameCountStr_.str("");
    //frameCountStr_.seekp(0);
    frameCountStr_ << "Health: " << player_.health_ ;
    splash_.draw(window_, frameCountStr_.str(), 10, 0);

    frameCountStr_.str("");
    //frameCountStr_.seekp(0);
    frameCountStr_ << "Frame: " << frameCount_ ;
    splash_.draw(window_, frameCountStr_.str(), 10, 20);

    frameCountStr_.str("");
    //frameCountStr_.seekp(0);
    frameCountStr_ << "Destroyed Enemy: " << player_.destroyed_enemy_count_ ;
    splash_.draw(window_, frameCountStr_.str(), 10, 40);

    if (!player_.is_alive_) {
        frameCountStr_.str("");
        frameCountStr_ << "<<<<  GAME OVER >>>>";
        splash_.draw(window_, frameCountStr_.str(), Common::W_WIDTH/3.0f, 0.0f);
    }
}

//
// collision between alien and player destroys alien and reduces
// player energy. If energy is 0, player dies also.
//

void Game::collision_update_aliens_player() {
    for(auto& alien : aliens_.vec_aliens_) {
        if (alien.is_alive_) {
            if (alien.sprite_.getGlobalBounds().intersects(player_.sprite_.getGlobalBounds())) {
                player_.has_collided_ = true;
                alien.has_collided_ = true;
                player_.destroyed_enemy_count_++;
                if (player_.health_ > 0) {
                    player_.health_ -= 1;
                }
                if (player_.health_ == 0) {
                    player_.is_alive_ = false;
                    state_ = GameContext::State::FINISHED;
                }
                alien.is_alive_ = false;
                break;
            }
        }
    }
}


//
// collisions decreases health of enemy ship (alien) and bullet not alive
//
void Game::collision_update_aliens_bullets() {
    for(auto& alien : aliens_.vec_aliens_) {
        for (auto& bullet : bullets_.vec_bullets_) {
            if (alien.is_alive_ && bullet.is_alive_) {
                // both bullet and alien are alive
                if (bullet.sprite_.getGlobalBounds().intersects(alien.sprite_.getGlobalBounds())) {
                    if (alien.health_ > 0) {
                        alien.health_ -= 1;
                    }
                    if (alien.health_ == 0) {
                        alien.is_alive_ = false;
                        player_.destroyed_enemy_count_++;
                    }
                    bullet.is_alive_ = false;
                }
            }
        }
    }
}
