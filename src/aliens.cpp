#include "stdlib.h"
#include "stdbool.h"

#include "aliens.h"
#include <vector>
#include "common.h"


//
// Private
//



//
// Public
//



Aliens::Aliens()  {
    // create alien pool
    for(uint8_t i=0; i<MAX_ALIEN_NUM; i++) {
        vec_aliens_.push_back(Alien {i});
    }
}



void Aliens::update() {
    for (auto& alien : vec_aliens_) {
        alien.update();
    }
}


void Aliens::render(sf::RenderWindow& window_) {
    for (auto& alien : vec_aliens_) {
        alien.render(window_);
    }
}
