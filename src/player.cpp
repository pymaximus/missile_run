// standard
#include <iostream>
#include <memory>
//#include <string>

// local
#include "common.h"
#include "player.h"
#include "player_image.h"



Player::Player() {

    x_ = static_cast<int>(Common::W_WIDTH/2.0f + 0.5);
    y_ = static_cast<int>(Common::W_HEIGHT * 0.9f + 0.5);

    has_collided_ = false;
    health_ = 9;
    is_alive_ = true;
    destroyed_enemy_count_ = 0;

    texture_ = std::make_shared<sf::Texture>();

    // load texture from header file and create sprite
    // file player.png
    //    player.png: PNG image data, 99 x 75, 8-bit/color RGBA, non-interlaced
    // convert player.png -define h:format=rgba -depth 8 -size 99x75 ../inc/player_image.h
    //
    if (!texture_->create(99,75)) {
        std::cout << "Error creating player texture" << std::endl;
    }
    texture_->update(player_image);
    // make sprite and set initial position.
    sprite_.setTexture(*texture_, true);
    sprite_.setPosition(x_, y_);
}


void Player::update(PlayerContext::Command cmd) {

    // accept no commands if dead
    if (!is_alive_) {
        return;
    }

    switch(cmd) {
    case PlayerContext::Command::LEFT: x_-=2;
        if (x_ < 0) {
            x_ = 0;
        }
        sprite_.setPosition(x_, y_);
        break;
    case PlayerContext::Command::RIGHT: x_+=2;
        if (x_ > Common::W_WIDTH - static_cast<int>(sprite_.getLocalBounds().width) + 0.5) {
            x_ = Common::W_WIDTH - static_cast<int>(sprite_.getLocalBounds().width + 0.5);
        }
        sprite_.setPosition(x_, y_);
        break;
    case PlayerContext::Command::FIRE: // TODO: handle fire
        
        break;
    default: std::cout << "Command not handled yet\n"; // no error
        break;
    }
}

void Player::render(sf::RenderWindow& window_) {
    window_.draw(sprite_);
}

void Player::input(void) {}
void Player::update(void) {}
