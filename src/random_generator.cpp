#include "common.h"
#include <random>

#include "random_generator.h"


// int RandomGenerator::get_random(const int & min, const int & max) {
//     static thread_local std::mt19937 generator;
//     std::uniform_int_distribution<int> distribution(min,max);
//     return distribution(generator);
// }

float RandomGenerator::get_random(const float min, const float max) {
    static thread_local std::mt19937 generator;
    std::uniform_real_distribution<float> distribution(min,max);
    return distribution(generator);
}
