// standard
#include "stdlib.h"
#include "stdbool.h"
#include <string>
#include <iostream>
#include <memory>
#include <cmath>

// local
#include "common.h"
#include "alien.h"
#include "enemy_image.h"
#include "random_generator.h"

Alien::Alien(uint16_t id) {

    id_ = id;
    reset();

    texture_ = std::make_shared<sf::Texture>();

    // load texture from header file and create sprite
    // file enemyShip.png
    //    enemyShip.png: PNG image data, 98 x 50, 8-bit/color RGBA, non-interlaced
    // convert enemyShip.png -define h:format=rgba -depth 8 -size 98x50 ../inc/enemy_image.h
    //
    if (!texture_->create(98,50)) {
        std::cout << "Error creating enemy ship texture" << std::endl;
    }
    texture_->update(enemy_image);

    // make sprite and set initial position.
    sprite_.setTexture(*texture_, true);
    sprite_.setPosition(x_, y_);
}

void Alien::update() {
    if (is_alive_) {
        x_ += vx_;
        // ensure x remains positive and bounded by display width
        x_ = fmod(x_ + Common::W_WIDTH, Common::W_WIDTH);

        y_ += vy_;
        if ( y_ > Common::W_HEIGHT - 1) {
            // heading off screen, mark as not alive, to be re-spawned later
            is_alive_ = false;
        }
        sprite_.setPosition(x_, y_);
        // alien low on health is red
        if (health_ < 2) {
            sprite_.setColor(sf::Color::Red);
        }
    } else {
        // alien needs re-spawning
        reset();
    }
}


void Alien::input() {}

void Alien::render(sf::RenderWindow& window_) {
    if (is_alive_) {
        window_.draw(sprite_);
    }
}


// Reset state, as these aliens can be re-used after dying.
void Alien::reset() {
    x_ = RandomGenerator::get_random(Common::W_WIDTH * 0.1f, Common::W_WIDTH * 0.9f);
    //std::cout << "x_ " << x_ << std::endl;
    y_ = RandomGenerator::get_random(0, Common::W_HEIGHT/10.0f);
    vx_ = RandomGenerator::get_random(-1,1);

    // vx can be {-1,0,1} when spawned
    vx_ = RandomGenerator::get_random(-1,1);
    vy_ = 1;
    is_alive_ = true;
    has_collided_ = false;
    health_ = 2;
    // set new position after resetting
    sprite_.setPosition(x_, y_);
    // reset back to normal un-damaged color
    sprite_.setColor(sf::Color(255,255,255));
}
