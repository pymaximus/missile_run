// standard
#include <iostream>
#include <math.h>

// local
#include "bullet.h"
#include "laser_red_image.h"

//
// constructor, used to create bullet pool. See Bullets::respawn()
//
Bullet::Bullet(uint16_t id) {

    // give id and load texture for sprite
    id_ = id;
    texture_ = std::make_shared<sf::Texture>();

    // must not be alive, only made alive when spawned (player shoots).
    is_alive_ = false;
    
    // load PNG image via C header file, using convert from ImageMagick
    // file laserRed.png
    //      laserRed.png: PNG image data, 9 x 33, 8-bit/color RGBA, non-interlaced
    //
    // convert laserRed.png -define h:format=rgba -depth 8 -size 9x33 ../inc/laser_red_image.h
    //
    // NOTE: Change array name in header file from MagickImage to suit.
    //

    // load texture and create sprite
    if (!texture_->create(9,33)) {
        std::cout << "Error creating bullet texture" << std::endl;
    }
    texture_->update(laser_red_image);
    // make sprite and set initial position.
    sprite_.setTexture(*texture_, true);
}

void Bullet::input(void) {}


void Bullet::update() {
    if (is_alive_) {
        x_ += vx_ * Common::FRAME_INTERVAL;
        // ensure x remains positive and bounded by display width
        x_ = fmod(x_ + Common::W_WIDTH, Common::W_WIDTH);
        y_ += vy_ * Common::FRAME_INTERVAL;
        if ( y_ < 1) {
            // heading off screen, mark as not alive, to be re-spawned later
            is_alive_ = false;
        }

        sprite_.setPosition(x_, y_);

    } else {
        // bullet needs re-spawning. This can be done by player
        // action at anytime. For example, pushing the fire button.
    }
}


void Bullet::render(sf::RenderWindow& window_) {
    window_.draw(sprite_);
}
