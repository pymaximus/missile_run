#include "stdlib.h"
#include "stdbool.h"
#include <iostream>
#include "bullet.h"
#include "bullets.h"
#include "player.h"


static constexpr uint32_t MAX_BULLET_NUM = 4;

Bullets::Bullets() {
    // create bullet pool
    // bullets not spawned en mass, only when shoot command has been detected
    for (uint8_t i=0; i< MAX_BULLET_NUM; i++) {
        vec_bullets_.push_back(Bullet {i});
    }


}


bool Bullets::respawn(const Player& player) {
    // prevent firing too quickly
    sf::Time elapsed_time = clock_.getElapsedTime();
    if (elapsed_time.asSeconds() < 0.5) {
        return false;
    } else {
        clock_.restart();
    }
    // find first dead bullet in pool and spawn
    for (auto& bullet : vec_bullets_) {
        if (! bullet.is_alive_) {
            // re-spawn bullet just in front of player, moving upwards
            bullet.x_ = player.x_ + static_cast<int>(player.sprite_.getLocalBounds().width/2.0 + 0.5) - 4;
            bullet.y_ = player.y_ - static_cast<int>(player.sprite_.getLocalBounds().height) + 50;
            bullet.vx_ = 0;
            bullet.vy_ = -4;
            bullet.is_alive_ = true;
            bullet.has_collided_ = false;

            return true;
        }
    }
    return false;
}


void Bullets::update() {
    for(auto& bullet : vec_bullets_) {
        bullet.update();
    }
}

void Bullets::render(sf::RenderWindow& window) {
    for(auto& bullet : vec_bullets_) {
        if (bullet.is_alive_) {
            bullet.render(window);
        }
    }
}

