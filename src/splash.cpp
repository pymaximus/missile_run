#include "splash.h"
#include <iostream>
#include "common.h"

#ifdef __APPLE__
#include "ResourcePath.hpp"
#endif

Splash::Splash() {

#ifdef __APPLE__
    const auto resource_path = resourcePath();
    std::cout << resource_path << std::endl;

    // Load font from a file
    if (!font_.loadFromFile(resource_path + fontPath))
    {
        std::cout << "Error loading font from " << fontPath << std::endl;
    }
    
#else
    // Load font from a file
    if (!font_.loadFromFile(fontPath))
    {
        std::cout << "Error loading font from " << fontPath << std::endl;
    }
    
#endif
    
    

    // select the font, size and color
    text_.setFont(font_);
    text_.setCharacterSize(20); // in pixels, not points!
    text_.setFillColor(sf::Color::White);

};

void Splash::draw(sf::RenderWindow& window, const std::string& text, float x, float y) {

    // set the string to display
    text_.setString(text);

    // set the text style
    //text_.setStyle(sf::Text::Bold | sf::Text::Underlined);

    text_.setPosition(x, y);
    //text_.setOrigin(text_.getLocalBounds().width/2.0f, text_.getLocalBounds().height/2.0f);

    window.draw(text_);
}
